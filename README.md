# Install YODA release-1.8

## Install plain version

```
VERSION=1.8
CUSTOMER=...
cd /var/ansiRods/yoda
ansible-playbook -vvv  --vault-password-file yoda-environments/vault.pw -i yoda-environments/${CUSTOMER} yoda-ansible-${VERSION}/playbook.yml --check

ansible-playbook -vvv  --vault-password-file yoda-environments/vault.pw -i yoda-environments/${CUSTOMER} yoda-ansible-${VERSION}/playbook.yml
```

## Customization

SURF customizations for YODA

Usage:

```
cd /var/ansiRods/yoda/yoda-deploy-surf-1.8
ANSIBLE_CONFIG=yoda-deploy-surf-${VERSION}/ansible.cfg ansible-playbook -vvv --vault-password-file ../yoda-environments/vault.pw -i ../yoda-environments/${CUSTOMER} playbook.yml
```

## Themes Packages

https://github.com/UtrechtUniversity/yoda/blob/development/docs/design/overview/theme-packages.md

